FROM alpine

RUN apk add gcc g++ clang nano bash strace

COPY .bashrc /root

CMD ["tail", "-f", "/dev/null"]

RUN apk add make
